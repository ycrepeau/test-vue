const { environment } = require('@rails/webpacker')

const webpack = require('webpack')
const aliasConfig = require('alias')
environment.config.merge(aliasConfig)

const coffee =  require('./loaders/coffee')
const vue =  require('./loaders/vue')
const haml_jsx =  require('./loaders/haml-jsx')

environment.loaders.append('vue', vue)
environment.loaders.append('coffee', coffee)
//environment.loaders.append('haml_jsx', haml_jsx)
module.exports = environment

