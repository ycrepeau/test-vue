module.exports = {
  test: /\.vue(\.erb)?$/,
  use: [{
    loader: 'haml-jsx-loader'
  }]
}
