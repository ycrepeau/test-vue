Rails.application.routes.draw do
  resources :todo_lists
  resources :todo_items
  get 'bonjour/index'
  root 'bonjour#index'


  namespace 'api' do
    namespace 'v1' do
      get 'info/index' => 'info#index'
      patch 'todo_items/update/:id/:position' => 'todo_items#update'
      get 'todo_items/index/:id/:position' => 'todo_items#index'
    end
  end

  get '/*path', to: 'bonjour#index', format: false
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
