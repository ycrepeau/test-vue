class CreateTodoItems < ActiveRecord::Migration[5.1]
  def change
    create_table :todo_items do |t|
      t.string :name
      t.integer :position

      t.timestamps
    end
  end
end
