class AddTodoListToTodoItems < ActiveRecord::Migration[5.1]
  def change
    add_reference :todo_items, :todo_list, index:true
  end
end
