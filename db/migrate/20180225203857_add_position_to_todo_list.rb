class AddPositionToTodoList < ActiveRecord::Migration[5.1]
  def change
    add_column :todo_lists, :position, :integer
  end
end
