module Api
  module V1
    class TodoItemsController < ApplicationController
      #skip_before_action :verify_authenticity_token
      def index
        @todo_item = TodoItem.find(params[:id])
        newPosition = params[:position].to_i
        render :json => "{newPosition: #{newPosition}, todo: #{@todo_item.to_json}}"
      end
      def update
        @todo_item = TodoItem.find(params[:id])
        newPosition = params[:position].to_i
        puts "{newPosition: #{newPosition}, todo: #{@todo_item.to_json}}"
        #@todo_item.remove_from_list
        @todo_item.set_list_position(newPosition)
        @todo_item.save()
        render :json => @todo_item.to_json
      end
    end
  end
end
