import VueRouter from 'vue-router'
import Carte from '../hello_app/carte.vue'
import Drag  from '../hello_app/drag.vue'
import Modal from '../hello_app/modal.vue'

#
# $$ - eventListener binder. 
# use:
# only on function in param: bind turbolinks:load to document.
# 1 param + function: bind the event provided to document
# 2 params + function: bind event (2nd arg) to target (1st arg)
#
$$ = (args..., func) ->
  if args.length == 0 
    target = window.document
    binding = 'turbolinks:load'
  if args.length == 1 
    target = window.document
    binding = args[0]
  if args >= 2 
    target = args[0]
    binding = args[1]

  target.addEventListener binding , func

firebaseConfig = {
  apiKey: "AIzaSyBHQh3lUb4uZAyUGRgQFZymDyGAcVDkgv8",
  authDomain: "test1-yc.firebaseapp.com",
  databaseURL: "https://test1-yc.firebaseio.com",
  projectId: "test1-yc",
  storageBucket: "",
  messagingSenderId: "376294597368"
}



database = () ->
  if sessionStorage.database?
    sessionStorage.database
  else
    console.log 'creating new database'
    firebase.initializeApp(firebaseConfig)
    sessionStorage.database = firebase.database()
    sessionStorage.database

router = new VueRouter ({
  mode: 'history',
  routes: [
    { path: '/',      component: Carte, name: 'home' }
    { path: '/drag',  component: Drag,  name: 'drag' }
    { path: '/modal', component: Modal, name: 'modal' }
    { path: '/carte', component: Carte, name: 'carte' }
  ]
  })

export {
  $$,
  firebaseConfig,
  database,
  router
  }

