# * eslint no-console: 0 *
# Run this example by adding <%= javascript_pack_tag 'hello_vue' %> (and
# <%= stylesheet_pack_tag 'hello_vue' %> if you have styles in your component)
# to the head of your layout file,
# like app/views/layouts/application.html.erb.
# All it does is render <div>Hello Vue</div> at the bottom of the page.

#import Vue from 'vue'
#import App from '../app.vue'
import "foundation"

import {$$, firebaseConfig, database, router} from './common'
import TurbolinksAdapter from 'vue-turbolinks'
import Vue from 'vue/dist/vue.esm'
import VueRouter from 'vue-router'
import App from '../hello_app/app.vue'

#
Vue.config.productionTip = false
Vue.use(TurbolinksAdapter)
Vue.use(VueRouter)
#


$$ 'turbolinks:render', ->
  #console.log 'turbolinks:render'

$$ 'turbolinks:before-visit', ->
  #console.log 'turbolinks:before-visit'

$$ 'turbolinks:before-render', (evt) ->
  #console.log 'turbolinks:before-render'
  #console.log evt.data.newBody.innerText

$$ -> #
  console.log 'turbolinks:load'
  
  app = new Vue {
    router
    el: '#webpack_entry_point'
    data: {
      message: "Pourrais-tu dire Allo?"
    }
    components: {
      App
    }

    beforeCreate: ->
      #console.log 'root before create'
      ###
      #console.log firebase.database()
      #users_db = firebase.database().ref('users/')
      users_db.on 'value', (snapshot) ->
        console.log "got something... }"
        console.log  snapshot.val()
      ###

    created: ->
      #console.log 'root created'

    beforeMount: ->
      #console.log 'root before mount'

    mounted: ->
      console.log 'root mounted'

    beforeUpdate: ->
      #console.log 'root before update'

    updated: ->
      console.log 'root updated'

    beforeDestroy: ->
      #console.log 'root before destroy'

    destroyed: ->
      console.log 'root destroyed'
  }
