     // Initialize Firebase
     var config = {
      apiKey: "AIzaSyBHQh3lUb4uZAyUGRgQFZymDyGAcVDkgv8",
      authDomain: "test1-yc.firebaseapp.com",
      databaseURL: "https://test1-yc.firebaseio.com",
      projectId: "test1-yc",
      storageBucket: "",
      messagingSenderId: "376294597368"
    };
    firebase.initializeApp(config);
    var database = firebase.database()

    function writeUserData(userId, name, email, imageUrl) {
      firebase.database().ref('users/' + userId).set({
        username: name,
        email: email,
        profile_picture : imageUrl
      });
    }

    //<!-- login -->
    //<script src="https://www.gstatic.com/firebasejs/ui/2.6.1/firebase-ui-auth__fr.js"></script>
    //<link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/2.6.1/firebase-ui-auth.css" />
    

    // FirebaseUI config.
    var uiConfig = {
    signInSuccessUrl: 'https://test1-yc.herokuapp.com',
    signInOptions: [
      // Leave the lines as is for the providers you want to offer your users.
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      //firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      //firebase.auth.TwitterAuthProvider.PROVIDER_ID,
      //firebase.auth.GithubAuthProvider.PROVIDER_ID,
      //firebase.auth.EmailAuthProvider.PROVIDER_ID,
      //firebase.auth.PhoneAuthProvider.PROVIDER_ID
    ],
    credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
    // Terms of service url.
    //tosUrl: 'https://test1-yc.herokuapp.com',

    callbacks: {
      signInSuccess: function(currentUser, credential, redirectUrl) {
        // User successfully signed in.
        // Return type determines whether we continue the redirect automatically
        // or whether we leave that to developer to handle.
        console.log('signInSuccess')
        return true;
      },
      uiShown: function() {
        // The widget is rendered.
        // Hide the loader.
        document.getElementById('loader').style.display = 'none';
      }
    },
  };

  window.uiConfig = uiConfig;
  