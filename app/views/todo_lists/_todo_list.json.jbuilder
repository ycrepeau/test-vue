json.extract! todo_list, :id, :name
json.todo_items todo_list.todo_items, :id, :name, :position
